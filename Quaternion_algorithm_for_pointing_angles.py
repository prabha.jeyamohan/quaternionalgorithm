import math
import numpy as np

lat = float(input('Enter the latitude of the turret : '))
lon = float(input('Enter the longitude of the turret : '))
alt= float(input('Enter the altitude of the turret above : '))
#Function to convert ships lat,long and altitude to ECEF co-ordinates
#def lla_to_ecef_1(lat, lon, alt):
    # see http://www.mathworks.de/help/toolbox/aeroblks/llatoecefposition.html
rad = np.float64(6378137.0)        # Radius of the Earth (in meters)
f = np.float64(1.0/298.257223563)  # Flattening factor WGS84 Model
cosLat = np.cos(lat)
sinLat = np.sin(lat)
FF     = (1.0-f)**2
C      = 1/np.sqrt(cosLat**2 + FF * sinLat**2)
S      = C * FF

xs = (rad * C + alt)*cosLat * np.cos(lon)
ys = (rad * C + alt)*cosLat * np.sin(lon)
zs = (rad * S + alt)*sinLat
   # return xs, ys, zs


# Ecef co-ordinates of the drone or target
xt = float(input('Enter x co-ordinate of drone: '))
yt = float(input('Enter y co-ordinate of drone: '))
zt = float(input('Enter z co-ordinate of drone: '))
pitch = input('Enter pitch value of the ship: ')
pitch = float(pitch) * (math.pi/180)
roll = input('Enter roll value of the ship: ')
roll = float(roll) * (math.pi/180)




#lla_to_ecef_1(lat, lon, alt)

#Line of sight vector calculation

x = float(xt-xs)
y = float(yt-ys)
z = float(zt-zs)



def func(pitch, roll, x, y, z):
    #add functionality to convert to x,y,z system to being relative to ship
    print('end')
#sigma = yaw
#fi = roll
#theta = pitch
def Ar(x, y, z, pitch, roll, yaw = 0):
    Ar = (-y*math.cos(yaw)*math.cos(roll)) + (x*math.sin(yaw)*math.cos(roll)) - (z*math.cos(pitch)*math.sin(roll)) - (x*math.cos(yaw)*math.sin(pitch)*math.sin(roll)) - (y*math.sin(yaw)*math.sin(pitch)*math.sin(roll))
    return Ar
def Br(x, y, z, pitch, roll, yaw = 0):
    Br = (z*math.cos(pitch)*math.cos(roll)) - (y*math.cos(yaw)*math.sin(roll) + x*math.sin(yaw)*math.sin(roll)) + (x*math.cos(yaw)* math.sin(pitch)*math.cos(roll)) + (y*math.sin(yaw)*math.sin(pitch)*math.cos(roll))
    return Br
def Ap(x, y, z, pitch, yaw = 0):
    Ap = (-z*math.sin(pitch)) + (x*math.cos(yaw)*math.cos(pitch)) + (y*math.sin(yaw)*math.cos(pitch)) #test -var in shell
    return Ap
def Bp(x, y, z, pitch, yaw = 0):
    Bp = ((z ** 2) * (math.cos(pitch) * math.cos(pitch))) + ((y ** 2) * (math.cos(yaw) * math.cos(yaw))) + ((x ** 2) * (math.sin(pitch) * math.sin(pitch)) * (math.cos(yaw) * math.cos(yaw))) + (y * z * math.sin(yaw) * math.sin(2*pitch)) + ((x ** 2) * (math.sin(yaw) * math.sin(yaw))) + ((y ** 2) * (math.sin(pitch) * math.sin(pitch)) * (math.sin(yaw) * math.sin(yaw))) + (x * z * math.cos(yaw) * math.sin(2*pitch)) - (2*x*y*math.sin(yaw) *math.cos(yaw)) + (x*y*(math.sin(pitch) * math.sin(pitch))*math.sin(2*yaw))
    return Bp
def calculateAzimuth(x, y, z, pitch):
    azimuth = math.atan(Ap(x, y, z, pitch) / Bp(x, y, z, pitch))
    azimuth = azimuth * (180 / math.pi)
    return azimuth
def calculateElevation(x, y, z, pitch, roll):
    elevation = math.atan(Ar(x, y, z, pitch, roll)/Br(x, y, z, pitch, roll))
    elevation = elevation * (180 / math.pi)
    return elevation
print (calculateAzimuth(x, y, z, pitch), "is the required azimuth angle" )
print(calculateElevation(x,y,z,pitch,roll), "is the required elevation angle")